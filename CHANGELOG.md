
## 0.0.9 [07-12-2024]

Add deprecation notice and metadata

See merge request itentialopensource/pre-built-automations/kafka-producer!14

2024-07-12 20:06:00 +0000

---

## 0.0.8 [07-06-2023]

* update master to 2023.1

See merge request itentialopensource/pre-built-automations/kafka-producer!13

---

## 0.0.7 [05-30-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/kafka-producer!10

---

## 0.0.6 [06-28-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/kafka-producer!9

---

## 0.0.5 [12-03-2021]

* Certified for 2021.2

See merge request itentialopensource/pre-built-automations/kafka-producer!8

---

## 0.0.4 [09-20-2021]

* Certifying for 2021.1

See merge request itentialopensource/pre-built-automations/kafka-producer!7

---

## 0.0.3 [09-20-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/kafka-producer!6

---

## 0.0.2 [11-12-2020]

* Patch/fix_JST

See merge request itentialopensource/pre-built-automations/kafka-producer!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n
