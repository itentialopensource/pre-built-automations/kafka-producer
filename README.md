<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 04-30-2024 and will be end of life on 04-30-2025. The capabilities of this Pre-Built have been replaced by the [Apache - Kafka - Library](https://gitlab.com/itentialopensource/pre-built-automations/apache-kafka-library)

<!-- Update the below line with your artifact name -->

# Kafka Producer

## Table of Contents

- [Overview](#overview)
- [Installation Prerequisites](#installation-prerequisites)
- [Requirements](#requirements)
- [Features](#features)
- [How to Install](#how-to-install)
- [How to Run](#how-to-run)
- [How to send encrypted messages using Avro](#how-to-send-encrypted-messages-using-avro)

## Overview

This Pre-Built Automation contains a workflow designed to send `non-encrypted` messages to a specific Kafka topic.

_Estimated Run Time_: < 1 min

## Installation Prerequisites

Users must satisfy the following pre-requisites:

- Itential Automation Platform: `^2022.1` 
- [Kafka Adapter](https://gitlab.com/itentialopensource/adapters/notification-messaging/adapter-kafka)

\* If using **Avro**, please follow this [guide](#how-to-send-encrypted-messages-using-avro).

## Requirements

This artifact requires the following:

- Kafka Server

## Features

The main benefits and features of the artifact are outlined below.

- Reusable workflow that can be altered according to one need.
- Allows zero-touch mode of operation.

## How to Install

To install the artifact:

- Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section.
- The artifact can be installed from within App-Admin_Essential. Simply search for the name of your desired artifact and click the install button.

## How to Run

To run the artifact:

- Navigate to Operations Manager in IAP and select the Kafka Producer Pre-Built.
- Select the existing manual trigger and complete the form that displays with the following details.

  | Form Element | Description |
  | :----------- | :---------- |
  | `Zero Touch` | Select checkbox to eliminate user interactions. |
  | `Kafka Adapter Id` | Name of your Kafka adapter. |
  | `Topic` | Kafka topic name to which messages should be sent. |
  | `Messages` | The messages that will be sent to the specified Kafka topic name. Message can be a string or a JSON object. For example: `hello`, `world` or `{"foo": "bar"}`.

## How to Send Encrypted Messages Using Avro

To encrypt messages using Avro:

- Make sure your [Kafka server](https://docs.confluent.io/current/quickstart/ce-quickstart.html) has a [Schema Registry](https://docs.confluent.io/current/schema-registry/schema_registry_tutorial.html) setup.
- Modify the reusable `Kafka Producer` workflow by clicking the **Merge** operation task and then adding the following keys.

  | Key | Description |
  | :-- | :---------- |
  | `simple` | Can be either `YES`(send message without encrypting) or `NO` (encrypt messages). |
  | `key_version` | Version number of the **Schema key** specified in the Schema registry. |
  | `value_version` | Version number of the **Schema value** specified in the Schema registry. |

- Specify the port address for the **"registry_url"** property in your your Kafka adapter configuration (required).

  `registry_url: <schema_registry_addr:port>`

### Additional Information

To learn more about Kafka Avro, use this [document](https://docs.confluent.io/current/schema-registry/serdes-develop/serdes-avro.html).
